const navOptionHandler = () => ({
  headerShown: true,
  headerTintColor: "#fff",
  headerTitleAlign:"left",
  headerTitleStyle: {
    marginLeft: 20,
  },
  headerStyle: {
    backgroundColor: "#000000",
    borderBottomWidth: 0.6,
   
  },
});

const navOptionHideHandler = () => ({
  headerShown: false
});

module.exports = {
  navOptionHandler: navOptionHandler,
  navOptionHideHandler: navOptionHideHandler
};
