import React from "react";
import { getLinkPreview } from "link-preview-js";
import PropTypes from "prop-types";
import {
  Image,
  Linking,
  Platform,
  Text,
  TouchableOpacity,
  View,
  ViewPropTypes,
  Dimensions
} from "react-native";

const REGEX = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/g;
const deviceWidth = Dimensions.get('window').width;
export default class LinkPreview extends React.PureComponent {



  constructor(props) {
    super(props);
    this.state = {
      isUri: false,
      linkTitle: undefined,
      linkDesc: undefined,
      linkFavicon: undefined,
      linkImg: undefined
    };
    this.getPreview(props.text);
  }

  getPreview = text => {
    const { onError, onLoad } = this.props;
    getLinkPreview(text)
      .then(data => {
        onLoad(data);
        this.setState({
          isUri: true,
          linkTitle: data.title ? data.title : undefined,
          linkDesc: data.description ? data.description : undefined,
          linkImg:
            data.images && data.images.length > 0
              ? data.images.find(function (element) {
                return (
                  element.includes(".png") ||
                  element.includes(".jpg") ||
                  element.includes(".jpeg")
                );
              })
              : undefined,
          linkFavicon:
            data.favicons && data.favicons.length > 0
              ? data.favicons[data.favicons.length - 1]
              : undefined
        });
      })
      .catch(error => {
        onError(error);
        this.setState({ isUri: false });
      });
  };

  componentDidUpdate(nextProps) {
    if (nextProps.text !== this.props.text) {
      this.getPreview(nextProps.text);
    } else if (nextProps.text == null) {
      this.setState({ isUri: false });
    }
  }

  _onLinkPressed = () => {
    Linking.openURL(this.props.text.match(REGEX)[0]);
  };

  renderImage = (
    imageLink,
    faviconLink,
    imageStyle,
    faviconStyle,
    imageProps,
    imageContainerStyle,
  ) => {
    return imageLink ? (

      <View style={imageContainerStyle}>
        <Image style={imageStyle} source={{ uri: imageLink }} />
      </View>

    ) : faviconLink ? (
      <View style={imageContainerStyle}>
        <Image
          style={imageStyle}
          source={{ uri: faviconLink }}

        />
      </View>
    ) : null;
  };
  renderText = (
    showTitle,
    title,
    description,
    textContainerStyle,
    titleStyle,
    descriptionStyle,
    titleNumberOfLines,
    descriptionNumberOfLines
  ) => {
    return (
      <View style={textContainerStyle}>
        {showTitle && (
          <Text numberOfLines={titleNumberOfLines} style={titleStyle}>
            {title}
          </Text>
        )}
      </View>
    );
  };

  renderDescription = (
    showTitle,
    title,
    description,
    textContainerStyle,
    titleStyle,
    descriptionStyle,
    titleNumberOfLines,
    descriptionNumberOfLines
  ) => {
    return (
      <View style={textContainerStyle}>
        {description && (
          <Text
            numberOfLines={descriptionNumberOfLines}
            style={descriptionStyle}
          >
            {description}
          </Text>
        )}
      </View>
    );
  };


  renderLinkPreview = (
    containerStyle,
    imageLink,
    faviconLink,
    imageStyle,
    imageContainerStyle,
    faviconStyle,
    showTitle,
    title,
    description,
    textContainerStyle,
    titleStyle,
    descriptionStyle,
    titleNumberOfLines,
    descriptionNumberOfLines,
    imageProps
  ) => {
    return (
      <TouchableOpacity
        style={[styles.containerStyle, containerStyle]}
        activeOpacity={0.9}
        onPress={() => this._onLinkPressed()}
      >
        {this.renderText(
          showTitle,
          title,
          description,
          textContainerStyle,
          titleStyle,
          descriptionStyle,
          titleNumberOfLines,
          descriptionNumberOfLines
        )}

        {this.renderImage(
          imageLink,
          faviconLink,
          imageContainerStyle,
          imageStyle,
          faviconStyle,
          imageProps
        )}
        {this.renderDescription(
          showTitle,
          title,
          description,
          textContainerStyle,
          titleStyle,
          descriptionStyle,
          titleNumberOfLines,
          descriptionNumberOfLines
        )}


      </TouchableOpacity>
    );
  };

  render() {
    const {
      text,
      containerStyle,
      imageStyle,
      imageContainerStyle,
      faviconStyle,
      textContainerStyle,
      title,
      titleStyle,
      titleNumberOfLines,
      descriptionStyle,
      descriptionNumberOfLines,
      imageProps
    } = this.props;
    return this.state.isUri
      ? this.renderLinkPreview(
        containerStyle,
        this.state.linkImg,
        this.state.linkFavicon,
        imageStyle,
        imageContainerStyle,
        faviconStyle,
        title,
        this.state.linkTitle,
        this.state.linkDesc,
        textContainerStyle,
        titleStyle,
        descriptionStyle,
        titleNumberOfLines,
        descriptionNumberOfLines,
        imageProps
      )
      : null;
  }
}

const styles = {
  containerStyle: {
    flexDirection: "column",
    flex: 1
  }
};

LinkPreview.defaultProps = {

  onLoad: () => { },
  onError: () => { },
  text: null,
  containerStyle: {

  },
  imageStyle: {
    width: 10,
    height: 10
  },

  imageContainerStyle: {
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
    height: 200,
    width: '95%',
    // width: deviceWidth * 0.8,
    resizeMode: 'contain',
    padding: 10
  },
  faviconStyle: {
    width: 40,
    height: 40,
    paddingRight: 10,
    paddingLeft: 10
  },
  textContainerStyle: {
    justifyContent: "flex-start",
    alignItems: "flex-start",
    padding: 10
  },
  title: true,
  titleStyle: {
    fontSize: 17,
    color: "#BD977E",
    marginRight: 10,
    alignSelf: "flex-start",
    fontFamily: "Helvetica",
    fontWeight: "bold"
  },
  titleNumberOfLines: 2,
  descriptionStyle: {
    fontSize: 14,
    color: "#FFF",
    marginRight: 10,
    alignSelf: "flex-start",
    fontFamily: "Helvetica"
  },
  descriptionNumberOfLines: Platform.isPad ? 4 : 3,
  imageProps: {}
};

LinkPreview.propTypes = {
  onLoad: PropTypes.func,
  onError: PropTypes.func,
  text: PropTypes.string,
  containerStyle: ViewPropTypes.style,
  imageStyle: ViewPropTypes.style,
  faviconStyle: ViewPropTypes.style,
  textContainerStyle: ViewPropTypes.style,
  title: PropTypes.bool,
  titleStyle: Text.propTypes.style,
  titleNumberOfLines: Text.propTypes.numberOfLines,
  descriptionStyle: Text.propTypes.style,
  descriptionNumberOfLines: Text.propTypes.numberOfLines
};
