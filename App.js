import React, { useState, useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import MainScreen from "./screens/MainScreen"
import LoginScreen from "./screens/LoginScreen"
import RegisterScreen from './screens/RegisterScreen';
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import * as headerConfigs from './helpers/header'
import AsyncStorage from '@react-native-community/async-storage';
import SplashScreen from 'react-native-splash-screen'
import { Text, View, ActivityIndicator } from 'react-native';

const StackApp = createStackNavigator();

const getData = async () => {
  try {
    const value = await AsyncStorage.getItem('user')
  
    if (value !== null) {
      return true;
    }
    else {
      return false;
    }

  } catch (e) {
    // error reading value
  }
}

export default function App() {

useEffect(()=> {
  SplashScreen.hide();
},[] );

  const [isLoggedin, setIsLoggedin] = React.useState("neutral");

  AsyncStorage.getItem('user').then((data) => {
    if (data != null) {
      setIsLoggedin("HomeScreen")
    }
    else {
      setIsLoggedin("Login")
    }
  })

  if (isLoggedin == "neutral")
    return (<View style={{ flex: 1,
      backgroundColor: "#3E3E3E",
      justifyContent: "center",
      alignItems: "center" }}>
    <ActivityIndicator size="large" color="#BD977E" />
    <Text style={{ fontSize: 16, color: 'white', marginTop: 20 }}>Loading Data...</Text>
  </View>)

  else
  return (
    <NavigationContainer>
      <StackApp.Navigator initialRouteName={isLoggedin}>
        <StackApp.Screen name="Login" component={LoginScreen} options={headerConfigs.navOptionHideHandler} />
        <StackApp.Screen name="HomeScreen" component={MainScreen} options={headerConfigs.navOptionHideHandler} />
        <StackApp.Screen name="User Registration" component={RegisterScreen} options={headerConfigs.navOptionHandler} />
      </StackApp.Navigator>
    </NavigationContainer>
  )

}



