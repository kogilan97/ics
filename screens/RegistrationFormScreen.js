import React, { useState, useEffect } from "react";
import { render } from "react-dom";
import { Text, View, Alert, ActivityIndicator, StyleSheet } from "react-native";
import { WebView } from "react-native-webview";
import * as Progress from "react-native-progress";
import AsyncStorage from "@react-native-community/async-storage";

const RegistrationComponent = ({ navigation }) => {
  const [progress, setProgress] = useState(0);
  const [isLoaded, setLoaded] = useState(false);
  const [loading, setLoading] = useState(true);
  const [url, setWebViewURL] = useState("");

  const registrationStatus = (data) => {
    Alert.alert(
      "Registration Status", 
      data,
      [
        {
          text: "Ok",
          onPress: () => {
            try {
              AsyncStorage.setItem("isUserProfile", "1");
              navigation.goBack();
            } catch (e) {
              console.log(e.toString());
            }
          },
        },
      ],
      { cancelable: false }
    );
  };

  useEffect(() => {
    AsyncStorage.getItem("user").then((data) => {
      setWebViewURL(
        "https://intercapitas.com/userregister.php?user_id=" + data
      );
      setLoading(false)
    });
  }, []);



  if (loading) {
    return (<View style={styles.loader}>
      <ActivityIndicator size="large" color="#BD977E" />
      <Text style={{ fontSize: 16, color: 'white', marginTop: 20 }}>Loading Data...</Text>
    </View>);

  }


  return (
    <View style={{ flex: 1 }}>
      {!isLoaded ? (
        <Progress.Bar
          color="orange"
          borderWidth={0}
          borderRadius={0}
          progress={progress}
          height={3}
          width={null}
        />
      ) : null}

      <WebView
        source={{ uri: url }}
        onMessage={(event) => {
          registrationStatus(event.nativeEvent.data);
        }}
        javaScriptEnabled={true}
        onLoadStart={(event) => {
          setLoaded(false);
        }}
        onLoadProgress={(event) => {
          setProgress(event.nativeEvent.progress);
        }}
        onLoadEnd={() => {
          setLoaded(true);
        }}
      />
    </View>
  );
};

export default RegistrationComponent;


const styles = StyleSheet.create({
  loader: {
    flex: 1,
    backgroundColor: "#3E3E3E",
    justifyContent: "center",
    alignItems: "center",
  }
})

