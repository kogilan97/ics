import "react-native-gesture-handler";
import * as React from "react";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import {
  View,
  StyleSheet,
  Button,
  Image,
  TouchableOpacity,
  Alert,
} from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import ReportScreen from "./ReportScreen";
import NewsScreen from "./NewsScreen";
import UserProfileScreen from "./UserProfileScreen";
import InvestmentSummaryScreen from "./InvestmentSummaryScreen"
import RegistrationFormScreen from "./RegistrationFormScreen"
import DetailedProfileScreen from "./DetailedProfileScreen"
import DetailedProfileScreen1 from "./DetailedProfileScreen1"
import PaymentUploadScreen from "./PaymentUploadScreen"
import * as headerConfigs from "../helpers/header";
import { replace } from "lodash";
import AsyncStorage from "@react-native-community/async-storage";

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

removeValue = async () => {
  AsyncStorage.getAllKeys()
  .then(keys => AsyncStorage.multiRemove(keys))
};

function ReportStack() {
  return (
    <Stack.Navigator
      initialRouteName="Report"
      screenOptions={headerConfigs.navOptionHandler}
    >
      <Stack.Screen
        name="Report"
        component={ReportScreen}
        options={{
          title: "Report",
        }}
      />
    </Stack.Navigator>
  );
}

function NewsStack() {
  return (
    <Stack.Navigator
      initialRouteName="News"
      screenOptions={headerConfigs.navOptionHandler}
    >
      <Stack.Screen
        name="News"
        component={NewsScreen}
        options={{
          title: "News",
        }}
      />
    </Stack.Navigator>
  );
}

const logOut = ({ replace }) => {
  Alert.alert(
    "Log Out",
    "Are you sure you want to logout now?",
    [
      {
        text: "Cancel",
        style: "cancel",
      },
      { text: "Log Out", onPress: () => logOut2(replace) },
    ],
    { cancelable: false }
  );
};

function logOut2(replace) {
  removeValue();
  replace("Login");
}

const UserProfileStack = ({ navigation }) => {
  return (
    <Stack.Navigator
      initialRouteName="User Profile"
      screenOptions={headerConfigs.navOptionHandler}
    >
      <Stack.Screen
        name="User Profile"
        component={UserProfileScreen}
        options={{
          title: "Profile",
          headerRight: () => (
            <TouchableOpacity
              style={styles.logOutView}
              onPress={() => {
                logOut(navigation);
              }}
            >
              <Image
                style={styles.logOut}
                source={require("../src/images/sign_out.png")}
              ></Image>
            </TouchableOpacity>
          ),
        }}
      />

      <Stack.Screen
        name="Investment_Summary"
        component={InvestmentSummaryScreen}
        options={{
          title: "Investment Summary"
        }}
      />
        <Stack.Screen
        name="Registration_Form"
        component={RegistrationFormScreen}
        options={{
          title: "Registration Form"
        }}
      />
        <Stack.Screen
        name="User_Details"
        component={DetailedProfileScreen1}
        options={{
          title: "User Details"
        }}
      />
        <Stack.Screen
        name="Upload_Payment"
        component={PaymentUploadScreen}
        options={{
          title: "Upload Payment"
        }}
      />
    </Stack.Navigator>
  );
};

export default class MainScreen extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Tab.Navigator
        initialRouteName="Feed"
        tabBarOptions={{
          inactiveTintColor: "#FFFFFF",
          activeTintColor: "#BD977E",
          style: {
            paddingTop: 5,
            paddingBottom: 5,
            height: 55,
            backgroundColor: "#000000",
          },
          labelStyle: {
            fontSize: 13,
          },
        }}
      >
        <Tab.Screen
          name="ReportStack"
          component={ReportStack}
          options={{
            tabBarLabel: "Report",
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons name="book" color={color} size={size} />
            ),
          }}
        />
        <Tab.Screen
          name="NewsStack"
          component={NewsStack}
          options={{
            tabBarLabel: "News",
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons name="bell" color={color} size={size} />
            ),
          }}
        />
        <Tab.Screen
          name="UserProfileStack"
          component={UserProfileStack}
          options={{
            tabBarLabel: "Profile",
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons
                name="account-box"
                color={color}
                size={size}
              />
            ),
          }}
        />
      </Tab.Navigator>
    );
  }
}

const styles = StyleSheet.create({
  refresh: {
    alignItems: "center",
    flex: 1,
    width: undefined,
    height: undefined,
  },

  refreshView: {
    marginRight: 20,
    height: 25,
    width: 25,
  },

  logOut: {
    alignItems: "center",
    flex: 1,
    width: undefined,
    height: undefined,
  },

  logOutView: {
    marginRight: 15,
    height: 35,
    width: 35,
  },
});



