import { View, Text, TextInput, StyleSheet } from "react-native";

import React from "react";

export default ({ title, value, editable, secureTextEntry, onSetCallback }) => {
  return (
    <View style={styles.action}>
      <Text style={styles.title}>{title}</Text>
      <TextInput
        editable={editable}
        numberOfLines={1}
        selectionColor="#BD977E"
        value={value}
        secureTextEntry={secureTextEntry}
        style={styles.textInput}
        onChangeText={onSetCallback}
      ></TextInput>
    </View>
  );
};

const styles = StyleSheet.create({
  action: {
    alignSelf: "center",
    flexDirection: "column",
    width: "80%",
    marginBottom: 15,
  },

  textInput: {
    padding: 0,
    paddingVertical: 2,
    color: "white",
    borderBottomWidth: 1.5,
    fontSize: 14,
    borderColor: "#BD977E",
  },
  title: {
    fontSize: 13,
    color: "#BD977E",
  },
});
