import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ImageBackground,
  ScrollView,
  TextInput,
  ToastAndroid,
  StyleSheet,
  ActivityIndicator,
  Platform,
  Alert,
} from "react-native";

import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Feather from "react-native-vector-icons/Feather";
import axios from "axios";
import BottomSheet from "reanimated-bottom-sheet";
import Animated from "react-native-reanimated";
import DefaultImage from "../src/images/default_user.png";
import ImagePicker from "react-native-image-crop-picker";
import AsyncStorage from "@react-native-community/async-storage";
import Clipboard from "@react-native-community/clipboard";

const DEFAULT_IMAGE = Image.resolveAssetSource(DefaultImage).uri;

const DetailedProfileScreen = ({ navigation }) => {
  const [image, setImage] = useState(DEFAULT_IMAGE);
  const [userId, setUserId] = useState("");
  const [referralCode, setReferralCode] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [loading, setLoading] = useState(false);
  const [initialImage, setInitialImage] = useState("");
  const bs = React.createRef();
  const fall = new Animated.Value(1);

  const takePhotoFromCamera = () => {
    ImagePicker.openCamera({
      compressImageMaxWidth: 300,
      compressImageMaxHeight: 300,
      cropping: true,
      compressImageQuality: 0.7,
    }).then((image) => {
      console.log(image);
      bs.current.snapTo(1);
      setImage(image.path);
    });
  };

  const choosePhotoFromLibrary = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 300,
      cropping: true,
      compressImageQuality: 0.7,
    }).then((image) => {
      console.log(image);
      bs.current.snapTo(1);
      setImage(image.path);
    });
  };

  useEffect(() => {
    AsyncStorage.multiGet([
      "user",
      "email",
      "firstName",
      "lastName",
      "referralCode",
      "profileImage",
    ]).then((data) => {
      setUserId(data[0][1]);
      setEmail(data[1][1]);
      setFirstName(data[2][1]);
      setLastName(data[3][1]);
      setReferralCode(data[4][1]);
      if (data[5][1]) {
        setImage(data[5][1]);
        setInitialImage(data[5][1]);
      }
    });
  }, []);

  const storeData = async (key, value) => {
    try {
      await AsyncStorage.setItem(key, value);
    } catch (e) {
      console.log(e.toString());
    }
  };

  const updateUserDetails = () => {
    console.log("Entering register User");
    if (!firstName.trim() || !lastName.trim()) {
      alert("Please Enter All The Required Details");
      return;
    }

    if (password.trim() != confirmPassword.trim()) {
      alert("Password and confirm password does not match");
      return;
    }

    if (password.trim().length < 8 && password.trim().length > 0) {
      alert("The password must be more than 8 characters.");
      return;
    }

    setLoading(true);

    var data = new FormData();

    data.append("firstname", firstName);
    data.append("lastname", lastName);
    data.append("user_id", userId);

    if (password.trim()) {
      data.append("password", password);
    }

    if (image != DEFAULT_IMAGE && image != initialImage) {
      console.log("Default Image : " + DEFAULT_IMAGE);
      console.log("Initial Image : " + initialImage);
      console.log("Entering2 : " + image);
      data.append("profile_image", {
        uri: image,
        name: "userProfile.jpg",
        type: "image/jpg",
      });
    }

    axios
      .post("http://live.intercapitas.com/api/userupdate", data)
      .then((response) => {
        setLoading(false);
        console.log("Entering Response");
        if (response.data.response.response == "200") {
          alert(response.data.response.message);
          storeData("firstName", firstName);
          storeData("lastName", lastName);
          if (image != DEFAULT_IMAGE && image != initialImage) {
            storeData("profileImage", image);
          }
          navigation.goBack();
        }
      })
      .catch((error) => {
        setLoading(false);
        alert("Something went wrong. Please try again. ");
      });
  };

  const renderInner = () => (
    <View style={styles.panel}>
      <View style={{ alignItems: "center" }}>
        <Text style={styles.panelTitle}>Upload Photo</Text>
        <Text style={styles.panelSubtitle}>Choose Your Profile Picture</Text>
      </View>
      <TouchableOpacity
        style={styles.panelButton}
        onPress={takePhotoFromCamera}
      >
        <Text style={styles.panelButtonTitle}>Take Photo</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.panelButton}
        onPress={choosePhotoFromLibrary}
      >
        <Text style={styles.panelButtonTitle}>Choose From Library</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.panelButton}
        onPress={() => bs.current.snapTo(1)}
      >
        <Text style={styles.panelButtonTitle}>Cancel</Text>
      </TouchableOpacity>
    </View>
  );

  const renderHeader = () => (
    <View style={styles.header}>
      <View style={styles.panelHeader}>
        <View style={styles.panelHandle} />
      </View>
    </View>
  );

  if (loading) {
    return (
      <View style={styles.loader}>
        <ActivityIndicator size="large" color="#BD977E" />
        <Text style={{ fontSize: 16, color: "white", marginTop: 20 }}>
          Updating User Details...
        </Text>
      </View>
    );
  }

  return (
    <ScrollView
      keyboardShouldPersistTaps="handled"
      contentContainerStyle={{
        flexGrow: 1,
      }}
    >
      <View style={styles.container}>
        <BottomSheet
          ref={bs}
          snapPoints={[330, 0]}
          renderContent={renderInner}
          renderHeader={renderHeader}
          initialSnap={1}
          callbackNode={fall}
          initialSnap={1}
          enabledContentGestureInteraction={false}
          enabledGestureInteraction={true}
        />
        <Animated.View
          style={{
            marginVertical: 20,
            opacity: Animated.add(0.1, Animated.multiply(fall, 1.0)),
          }}
        >
          <View style={{ alignItems: "center" }}>
            <TouchableOpacity onPress={() => bs.current.snapTo(0)}>
              <View
                style={{
                  height: 100,
                  width: 100,
                  borderRadius: 15,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <ImageBackground
                  source={{
                    uri: image,
                  }}
                  style={{ height: 100, width: 100 }}
                  imageStyle={{ borderRadius: 15 }}
                >
                  <View
                    style={{
                      flex: 1,
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <Icon
                      name="camera"
                      size={35}
                      color="#fff"
                      style={{
                        opacity: 0.7,
                        alignItems: "center",
                        justifyContent: "center",
                        borderWidth: 1,
                        borderColor: "#fff",
                        borderRadius: 10,
                      }}
                    />
                  </View>
                </ImageBackground>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onLongPress={() => {
                Clipboard.setString(referralCode);
                alert("Your referral code has been copied to clipboard");
              }}
              style={{
                borderColor: "#fff",
                borderWidth: 0.5,
                padding: 10,
                marginVertical: 15,
                flexDirection: "row",
              }}
            >
              <Text
                style={{
                  fontSize: 15,
                  color: "#fff",
                }}
              >
                Your Referral Code :{" "}
              </Text>
              <Text
                style={{
                  fontSize: 15,
                  fontStyle: "italic",
                  fontWeight: "bold",
                  color: "#fff",
                }}
              >
                {referralCode}
              </Text>
            </TouchableOpacity>
          </View>

          <View style={styles.action}>
            <FontAwesome name="user-o" color="#fff" size={20} />
            <TextInput
              placeholder="First Name"
              selectionColor="#BD977E"
              placeholderTextColor="#7e7e7e"
              value={firstName}
              onChangeText={(value) => {
                setFirstName(value);
              }}
              autoCorrect={false}
              style={[
                styles.textInput,
                {
                  color: "#fff",
                },
              ]}
            />
          </View>
          <View style={styles.action}>
            <FontAwesome name="user-o" color="#fff" size={20} />
            <TextInput
              placeholder="Last Name"
              value={lastName}
              placeholderTextColor="#7e7e7e"
              selectionColor="#BD977E"
              autoCorrect={false}
              onChangeText={(value) => {
                setLastName(value);
              }}
              style={[
                styles.textInput,
                {
                  color: "#fff",
                },
              ]}
            />
          </View>

          <View style={styles.action}>
            <FontAwesome name="envelope-o" color="#fff" size={20} />
            <TextInput
              placeholder="Email"
              value={email + "   (Not Editable)"}
              editable={false}
              placeholderTextColor="#7e7e7e"
              selectionColor="#BD977E"
              keyboardType="email-address"
              autoCorrect={false}
              style={[
                styles.textInput,
                {
                  color: "#A9A9A9",
                },
              ]}
            />
          </View>

          <View style={styles.action}>
            <Feather name="lock" color="#fff" size={20} />
            <TextInput
              secureTextEntry
              onChangeText={(value) => {
                setPassword(value);
              }}
              placeholder="Password"
              placeholderTextColor="#7e7e7e"
              selectionColor="#BD977E"
              autoCorrect={false}
              style={[
                styles.textInput,
                {
                  color: "#fff",
                },
              ]}
            />
          </View>

          <View style={styles.action}>
            <Feather name="lock" color="#fff" size={20} />
            <TextInput
              placeholder="Confirm Password"
              placeholderTextColor="#7e7e7e"
              onChangeText={(value) => {
                setConfirmPassword(value);
              }}
              secureTextEntry
              selectionColor="#BD977E"
              autoCorrect={false}
              style={[
                styles.textInput,
                {
                  color: "#fff",
                },
              ]}
            />
          </View>

          <TouchableOpacity
            onPress={() => {
              updateUserDetails();
            }}
            style={styles.commandButton}
          >
            <Text style={styles.panelButtonTitle}>Update Profile</Text>
          </TouchableOpacity>
        </Animated.View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#3E3E3E",
    flex: 1,
  },
  commandButton: {
    alignSelf: "center",
    width: "45%",
    backgroundColor: "#BD977E",
    borderRadius: 25,
    height: 45,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    marginBottom: 20,
  },
  panel: {
    padding: 20,
    backgroundColor: "#FFFFFF",
    paddingTop: 20,
  },
  header: {
    backgroundColor: "#FFFFFF",
    shadowColor: "#333333",
    shadowOffset: { width: -1, height: -3 },
    shadowRadius: 2,
    shadowOpacity: 0.4,
    // elevation: 5,
    paddingTop: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  panelHeader: {
    alignItems: "center",
  },
  panelHandle: {
    width: 40,
    height: 8,
    borderRadius: 4,
    backgroundColor: "#00000040",
    marginBottom: 10,
  },
  panelTitle: {
    fontSize: 27,
    height: 35,
  },
  panelSubtitle: {
    fontSize: 14,
    color: "gray",
    height: 30,
    marginBottom: 10,
  },
  panelButton: {
    padding: 13,
    borderRadius: 10,
    backgroundColor: "#BD977E",
    alignItems: "center",
    marginVertical: 7,
  },
  panelButtonTitle: {
    fontSize: 17,
    fontWeight: "bold",
    color: "white",
  },
  action: {
    alignSelf: "center",
    flexDirection: "row",
    width: "80%",
    borderBottomWidth: 1.5,
    borderColor: "#BD977E",
    height: 50,
    marginBottom: 20,
    justifyContent: "center",
    alignItems: "center",
    padding: 10,
  },
  actionError: {
    flexDirection: "row",
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#FF0000",
    paddingBottom: 5,
  },
  textInput: {
    flex: 1,
    height: 50,
    marginStart: 10,
    color: "white",
  },

  loader: {
    flex: 1,
    backgroundColor: "#3E3E3E",
    justifyContent: "center",
    alignItems: "center",
  },
});

export default DetailedProfileScreen;
