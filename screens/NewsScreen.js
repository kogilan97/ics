import * as React from 'react';
import {
  StyleSheet,
  View,
  Text,
  SafeAreaView,
  ScrollView,
  ActivityIndicator,
  FlatList,
  RefreshControl} from 'react-native';

import LinkPreview from '../helpers/link_preview';
import NetworkUtils from "../helpers/networkUtils";
import LinkPreview2 from '../helpers/link_preview2';
import axios from "axios";


function extractMetaData(value) {

  return (<LinkPreview text={value} />);
}
function extractMetaData2(value) {
  return (<LinkPreview2 text={value} />);
}


export default class NewsScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      urls: [],
      // urls:[],
      newTableData: [],
      loading: true,
      isFetching: true,
      isConnected: true
    }
  }



  onRefresh = () => {
    this.setState({ isFetching: true, loading: true, newTableData: [] });
    this.fetchData();
  };

  async fetchData() {

    const isConnected = await NetworkUtils.isNetworkAvailable()
    if (isConnected) {

      axios
      .get("http://live.intercapitas.com/api/news")
      .then((response) => {
        setTimeout(() => {
          this.setState({
            loading:false
           })
     
          if (response.data.News !=null) {
            var newTableData = []
            response.data.News.forEach(function (item, index) {
              if (index == 0) {
                newTableData.push({ "key": index.toString(), "preview": extractMetaData(item.url) });
              }
              else {
                newTableData.push({ "key": index.toString(), "preview": extractMetaData2(item.url) });
              }
            });
            this.setState({
              isFetching: false,
              newTableData: newTableData,
              loading: false,
              isConnected: true
            })
          } 
        }, 2000)
      })
      .catch((error) => {
        this.setState({
          loading:false
         })
        alert("Something went wrong. Please try again. ");
      });
    }
    else {
      this.setState({
        loading: false,
        isFetching: false,
        isConnected: false
      })
    }
  }



  componentDidMount() {
    this.setState({
      newTableData: [],
      loading: true,
      isFetching: true
    })

    this.fetchData()
  }


  render() {
    const state = this.state;

    if (state.loading) {
      return (<View style={styles.loader}>
        <ActivityIndicator size="large" color="#BD977E" />
        <Text style={{ fontSize: 16, color: 'white', marginTop: 20 }}>Loading Data...</Text>
      </View>);

    }

    else if (!state.isConnected) {
      return (
      
        <SafeAreaView style={{ flex: 1, backgroundColor: '#3E3E3E' }}>
        
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh}
                title="Pull to refresh"
              />
            }>
            <View style={styles.loader}>
              <Text style={{ fontSize: 16, color: 'white', textAlign: 'center', marginTop: 20 }}>Please check your internet connection and pull down to refresh</Text>
            </View>
          </ScrollView>
        </SafeAreaView>
      )
    }

    else
      if (state.loading == false && state.newTableData.length == 0) {

        return (
          <View style={styles.loader}>
            <Text style={{ fontSize: 16, color: 'white', textAlign: 'center', marginTop: 20 }}>There are no news available right now.</Text>
          </View>
        )
      }
      else

        if (state.loading == false && state.newTableData.length > 0) {
          return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#3E3E3E' }}>


              <View style={{ flex: 1, paddingTop: 10, paddingLeft: 10, paddingRight: 10 }}>
                <View
                  style={{
                    flex: 1
                  }}>

                  {/*     
          <View style={styles.newsPanel}>
            <View style={styles.newsImageView}>

              <Image
                style={styles.newsImage}
                source={{ uri: 'https://res.cloudinary.com/demo/image/upload/q_90/happy_dog.jpg' }}>
              </Image>
            </View>
            <View style={styles.newsTextView}>

              <Text style={styles.newsTitle} 
              ellipsizeMode='tail' numberOfLines={2}>
                react native text input limit characters. If set
               multiline as true, React Native will accept    
              </Text>
              
              <Text style={styles.newsDescription} 
              ellipsizeMode='tail' numberOfLines={5} >     
              react native text input limit characters. If set
               multiline as true, React Native will accept
               react native text input limit characters. If set
               multiline as true, React Native will accept
               react native text input limit characters. If set
               multiline as true, React Native will accept  

              </Text>

            </View>
         
          </View>

          <RNUrlPreview text={"https://www.google.com/"}/>
            <RNUrlPreview text={"https://www.youtube.com/watch?v=Kmiw4FYTg2Uwe"}/>
            <RNUrlPreview text={"https://www.tesla.com/"}/>

      <FlatList
                    data={state.newTableData}
                    renderItem={({ item }) => 
                  
                  {item.preview}
                  
                  }

                  ></FlatList>
          */}

                  <FlatList
                    data={state.newTableData}
                    onRefresh={() => this.onRefresh()}
                    refreshing={this.state.isFetching}
                    renderItem={({ item }) =>
                      <View>{item.preview}</View>
                    }
                  />

                </View>

              </View>

            </SafeAreaView>
          )
        }

  }
}

const styles = StyleSheet.create({

  newsImage: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'contain'
  },
  newsImageView: {
    justifyContent: "flex-start",
    flex: 0.45,
    paddingBottom: 5,
  },
  newsTextView: {
    paddingLeft: 7,
    flex: 0.55,
    flexDirection: "column",

  },

  newsTitle: {
    fontWeight: "bold",
    fontSize: 19,
    flexDirection: "column",
    color: "#FFFFFF"
  },

  newsDescription: {
    flexDirection: "column",
    color: "#FFFFFF"

  },

  newsPanel: {
    flexDirection: "row",
    height: 150,
    marginBottom: 5,
    borderColor: "white",
    borderBottomWidth: 0.2,
  },

  loader: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: '#3E3E3E'
  },




});