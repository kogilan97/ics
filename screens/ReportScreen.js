import * as React from "react";
import {
  TouchableOpacity,
  StyleSheet,
  View,
  Text,
  Image,
  SafeAreaView,
  FlatList,
  ScrollView,
  RefreshControl,
  TouchableWithoutFeedback,
  Platform,
  ActivityIndicator,
} from "react-native";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import axios from "axios";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import NetworkUtils from "../helpers/networkUtils";
import AsyncStorage from "@react-native-community/async-storage";
import RNPickerSelect from "react-native-picker-select";
import Icon from "react-native-vector-icons/Feather";
import { Picker } from "@react-native-picker/picker";
import DropDownPicker from "react-native-dropdown-picker";

function profitColumnTextColour(value) {
  var res = value.charAt(0);
  if (res == "-") {
    return "negative";
  } else {
    return "positive";
  }
}

export default class ReportScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fundItems: [],
      username: "",
      total: "",
      profit: "",
      loading: false,
      isConnected: true,
      isFetching: true,
      newTableData: [],
      corporateData1: [],
      corporateData: [
        {
          key: "a",
          image:
            "https://www.vhv.rs/dpng/d/22-221315_react-native-logo-hd-png-download.png",
          name: "Corporate Name1",
          profit: "First Item",
          date: "21-Apr-2020",
          currency: "1,000",
          buy: "1,000",
          trade_price: "1000",
          sell: "277.55",
          gross_pl: "1000",
          interest: "1000",
          brokerage_fee: "1000",
        },
        {
          key: "b",
          image:
            "https://www.vhv.rs/dpng/d/22-221315_react-native-logo-hd-png-download.png",
          name: "Corporate Name21",
          profit: "First Item",
          date: "21-Apr-2020",
          currency: "1,000",
          buy: "1,000",
          trade_price: "1000",
          sell: "277.55",
          gross_pl: "1000",
          interest: "1000",
          brokerage_fee: "1000",
        },
      ],
    };
  }

  onRefresh = () => {
    this.setState({ isFetching: true, loading: true, newTableData: [] });
    this.getFundNames();
  };

  async getFundNames() {
    const isConnected = await NetworkUtils.isNetworkAvailable();
    AsyncStorage.getItem("user").then((data) => {
      if (data != null) {
        if (isConnected) {
          axios
            .post("http://live.intercapitas.com/api/funds", {
              user_id: data,
            })
            .then((response) => {
              let fundDetails = response.data.Funds;
              let serviceItems = [];
              let firstFund;

              if (fundDetails.length > 0) {
                serviceItems = fundDetails.map((item, index) => {
                  const container = {};
                  if (index == 0) {
                    firstFund = item.funds_id;
                    container.selected = true;
                  } else {
                    container.selected = false;
                  }
                  container.item = item.funds_id;
                  container.label = item.funds_name;
                  return container;
                });
                // serviceItems=[];
                this.setState({
                  fundItems: serviceItems,
                  loading: true,
                  isFetching: false,
                  isConnected: true,
                });

                this.getApiData(firstFund);
              } else {
                this.setState({
                  fundItems: serviceItems,
                  loading: false,
                  isFetching: false,
                  isConnected: true,
                });
              }
            })
            .catch((error) => {
              this.setState({
                loading: false,
                isFetching: false,
                isConnected: true,
              });
              alert("Something went wrong. Please try again. ");
            });
        } else {
          this.setState({
            loading: false,
            isFetching: false,
            isConnected: false,
          });
        }
      }
    });
  }

  async getApiData(fundValue) {
    console.log("test123" + fundValue);
    convertedItem = this.state.fundItems.map((item, index) => {
      const container = {};

      if (item.item == fundValue) {
        container.selected = true;
      } else {
        container.selected = false;
      }
      container.item = item.item;
      container.label = item.label;
      return container;
    });
    this.setState({
      fundItems: convertedItem,
      loading: true,
      isFetching: true,
      newTableData: [],
    });
    //  this.getFundNames()
    const isConnected = await NetworkUtils.isNetworkAvailable();

    AsyncStorage.getItem("user").then((data) => {
      if (data != null) {
        console.log(data);
        if (isConnected) {
          axios
            .post("http://live.intercapitas.com/api/userreport", {
              user_id: data,
              funds_id: fundValue,
            })
            .then((response) => {
              let fundDetails = response.data.response[0].funds;
              //   console.log(fundDetails);
              //    let refinedFundDetails = [];

              var username,
                total = parseFloat(response.data.response[0].total),
                profit;
              let newFundDetails = [];
              //     username = response.data.per_page;
              //     total = response.data.total;
              //     profit = response.data.total;

              var newFundObject = {};
              var flag = 0;
              for (var i = 0; i < fundDetails.length; i++) {
                if (flag == 0) {
                  newFundObject["id"] = fundDetails[i].id;
                  newFundObject["logo_url"] = fundDetails[i].logo_url;
                  newFundObject["nett_pl"] = fundDetails[i].nett_pl;
                  newFundObject["nett_pl_status"] = profitColumnTextColour(
                    fundDetails[i].nett_pl
                  );
                  newFundObject["currency"] = fundDetails[i].currency;
                  newFundObject["primary_date"] = fundDetails[i].date;
                  newFundObject["short_sell"] = fundDetails[i].short_sell;
                  newFundObject["primary_trade_price"] =
                    fundDetails[i].trade_price;
                  newFundObject["primary_total_value"] = "kiv";
                  newFundObject["primary_brokerage_fee"] =
                    fundDetails[i].brokerage_fee;
                  newFundObject["gross_pl"] = fundDetails[i].gross_pl;
                  newFundObject["description"] = fundDetails[i].description;
                  newFundObject["gross_pl_status"] = profitColumnTextColour(
                    fundDetails[i].gross_pl
                  );
                  newFundObject["total_s_b_fee"] = fundDetails[i].total_s_b_fee;
                  flag = 1;
                } else {
                  newFundObject["secondary_date"] = fundDetails[i].date;
                  newFundObject["buy"] = fundDetails[i].buy;
                  newFundObject["secondary_trade_price"] =
                    fundDetails[i].trade_price;
                  newFundObject["secondary_total_value"] = "kiv2";
                  newFundObject["secondary_brokerage_fee"] =
                    fundDetails[i].brokerage_fee;
                  newFundDetails.push(newFundObject);
                  flag = 0;
                  newFundObject = {};
                }
                //   total = total + parseFloat(fundDetails[i].nett_pl);
                //    refinedFundDetails.push([firstColumnTextColour(fundDetails[i].email),fundDetails[i].first_name,fundDetails[i].last_name]);

                //  refinedFundDetails.push(fundDetails[i].funds_name, fundDetails[i].currency + " " + fundDetails[i].total, fundDetails[i].currency + " " + fundDetails[i].profit);
              }

              total = total.toFixed(2);
              this.setState({
                newTableData: newFundDetails,
                //     username: username,
                total: total,
                //      profit: profit,
                loading: false,
                isFetching: false,
                isConnected: true,
              });
            })
            .catch((error) => {
              this.setState({
                loading: false,
                isFetching: false,
                isConnected: true,
              });
              alert("Something went wrong. Please try again. ");
            });
        } else {
          this.setState({
            loading: false,
            isFetching: false,
            isConnected: false,
          });
        }
      }
    });
  }

  componentDidMount() {
    this.setState({
      loading: true,
      isFetching: true,
    });
    this.getFundNames();
  }

  render() {
    const state = this.state;

    if (state.loading) {
      return (
        <View style={styles.loader}>
          <ActivityIndicator size="large" color="#BD977E" />
          <Text style={{ fontSize: 16, color: "white", marginTop: 20 }}>
            Loading Data...
          </Text>
        </View>
      );
    } else if (!state.isConnected) {
      return (
        <SafeAreaView style={{ flex: 1, backgroundColor: "#3E3E3E" }}>
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh}
                title="Pull to refresh"
              />
            }
          >
            <View style={styles.loader}>
              <Text
                style={{
                  fontSize: 16,
                  color: "white",
                  textAlign: "center",
                  marginTop: 20,
                }}
              >
                Please check your internet connection and pull down to refresh
              </Text>
            </View>
          </ScrollView>
        </SafeAreaView>
      );
    } else if (state.loading == false && state.fundItems.length == 0) {
      return (
        <SafeAreaView style={{ flex: 1, backgroundColor: "#3E3E3E" }}>
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh}
                title="Pull to refresh"
              />
            }
          >
            <View style={styles.loader}>
              <Text
                style={{
                  fontSize: 16,
                  color: "white",
                  textAlign: "center",
                  marginTop: 20,
                }}
              >
                There are no funds available right now.
              </Text>
            </View>
          </ScrollView>
        </SafeAreaView>
      );
    } else if (state.loading == false && state.newTableData.length > 0) {
      return (
        <SafeAreaView style={{ flex: 1, backgroundColor: "#3E3E3E" }}>
          <View
            style={{
              flex: 1,
              paddingTop: 10,
              paddingLeft: 10,
              paddingRight: 10,
            }}
          >
            <View
              style={{
                flex: 1,
              }}
            >
              <DropDownPicker
                items={state.fundItems}
                //   placeholder="Please select a fund"
                placeholderStyle={{
                  color: "#FFFFFF",
                  alignSelf: "center",
                  alignContent: "center",
                  alignItems: "center",
                }}
                selectedLabelStyle={{
                  color: "#FFFFFF",
                }}
                arrowColor="#FFFFFF"
                containerStyle={{
                  marginTop: 10,
                  marginBottom: 5,
                  height: 40,
                  width: Platform.isPad ? 300 : 250,
                  alignSelf: "center",
                }}
                style={{ backgroundColor: "#3E3E3E" }}
                itemStyle={{
                  justifyContent: "flex-start",
                  color: "#000000",
                }}
                dropDownStyle={{ backgroundColor: "#ffffff" }}
                onChangeItem={(item) => {
                  console.log("test131 : " + item.item);
                  this.getApiData(item.item);
                }}
              />

              <View style={styles.topPanelView}>
                {/*
                <View style={styles.individualTopPanelView}>
                  <Text style={styles.individualTopPanelViewPriceText}>
                    RM 1,200.00
                  </Text>
                  <Text style={styles.individualTopPanelViewTitleText}>
                    Available
                  </Text>
                </View>
                */}
                <View style={styles.individualTopPanelView}>
                  <Text style={styles.individualTopPanelViewPriceText}>
                    {state.total}
                  </Text>
                  <Text style={styles.individualTopPanelViewTitleText}>
                    Total Allocated
                  </Text>
                </View>
                {/*
                <View style={styles.individualTopPanelView}>
                  <Text style={styles.individualTopPanelViewPriceText}>
                    RM 1,200.00
                  </Text>
                  <Text style={styles.individualTopPanelViewTitleText}>
                    Total
                  </Text>
                </View>
                 */}
              </View>

              <FlatList
                data={state.newTableData}
                keyExtractor={(item) => item.id.toString()}
                onRefresh={() => this.onRefresh()}
                refreshing={this.state.isFetching}
                renderItem={({ item }) => (
                  <View style={styles.corporatePanel}>
                    <View style={styles.corporatePanelFirstRow}>
                      <View style={styles.imageView}>
                        <Image
                          style={styles.image}
                          source={{ uri: item.logo_url }}
                        ></Image>
                      </View>

                      <Text
                        ellipsizeMode="tail"
                        numberOfLines={2}
                        style={styles.corporateName}
                      >
                        {item.description}
                      </Text>

                      <View style={styles.corporateProfitDate}>
                        <Text
                          ellipsizeMode="tail"
                          numberOfLines={1}
                          style={styles[item.nett_pl_status]}
                        >
                          {item.currency + " " + item.nett_pl}
                        </Text>

                        <Text
                          ellipsizeMode="tail"
                          numberOfLines={1}
                          style={styles.corporateDate}
                        >
                          {"Currency: " + item.currency}
                        </Text>
                      </View>
                    </View>
                    <View style={styles.corporatePanelSecondRow}>
                      <View style={styles.corporatePanelOtherRows1}>
                        <Text style={styles.corporatePanelOtherRowsTitle}>
                          Date
                        </Text>
                        <Text style={styles.corporatePanelOtherRowsValues}>
                          {item.primary_date}
                        </Text>
                      </View>
                      <View style={styles.corporatePanelOtherRows2}>
                        <Text style={styles.corporatePanelOtherRowsTitle}>
                          Date
                        </Text>
                        <Text style={styles.corporatePanelOtherRowsValues}>
                          {item.secondary_date}
                        </Text>
                      </View>
                    </View>
                    <View style={styles.corporatePanelThirdRow}>
                      <View style={styles.corporatePanelOtherRows1}>
                        <Text style={styles.corporatePanelOtherRowsTitle}>
                          Short Sell
                        </Text>
                        <Text style={styles.corporatePanelOtherRowsValues}>
                          {item.short_sell}
                        </Text>
                      </View>
                      <View style={styles.corporatePanelOtherRows2}>
                        <Text style={styles.corporatePanelOtherRowsTitle}>
                          Buy
                        </Text>
                        <Text style={styles.corporatePanelOtherRowsValues}>
                          {item.buy}
                        </Text>
                      </View>
                    </View>
                    <View style={styles.corporatePanelFourthRow}>
                      <View style={styles.corporatePanelOtherRows1}>
                        <Text style={styles.corporatePanelOtherRowsTitle}>
                          Trade Price
                        </Text>
                        <Text style={styles.corporatePanelOtherRowsValues}>
                          {item.primary_trade_price}
                        </Text>
                      </View>
                      <View style={styles.corporatePanelOtherRows2}>
                        <Text style={styles.corporatePanelOtherRowsTitle}>
                          Trade Price
                        </Text>
                        <Text style={styles.corporatePanelOtherRowsValues}>
                          {item.secondary_trade_price}
                        </Text>
                      </View>
                    </View>
                    <View style={styles.corporatePanelFifthRow}>
                      <View style={styles.corporatePanelOtherRows1}>
                        <Text style={styles.corporatePanelOtherRowsTitle}>
                          Total Value
                        </Text>
                        <Text style={styles.corporatePanelOtherRowsValues}>
                          {item.primary_total_value}
                        </Text>
                      </View>
                      <View style={styles.corporatePanelOtherRows2}>
                        <Text style={styles.corporatePanelOtherRowsTitle}>
                          Total Value
                        </Text>
                        <Text style={styles.corporatePanelOtherRowsValues}>
                          {item.secondary_total_value}
                        </Text>
                      </View>
                    </View>
                    <View style={styles.corporatePanelSixthRow}>
                      <View style={styles.corporatePanelOtherRows1}>
                        <Text
                          style={styles.corporatePanelOtherRowsTitle}
                        ></Text>
                        <Text
                          style={styles.corporatePanelOtherRowsValues}
                        ></Text>
                      </View>
                      <View style={styles.corporatePanelOtherRows2}>
                        <Text style={styles.corporatePanelOtherRowsTitle}>
                          Gross P/L
                        </Text>
                        <Text style={styles.corporatePanelOtherRowsValues}>
                          {item.gross_pl}
                        </Text>
                      </View>
                    </View>
                    <View style={styles.corporatePanelSeventhRow}>
                      <View style={styles.corporatePanelOtherRows1}>
                        <Text style={styles.corporatePanelOtherRowsTitle}>
                          Brokerage Fee
                        </Text>
                        <Text style={styles.corporatePanelOtherRowsValues}>
                          {item.primary_brokerage_fee}
                        </Text>
                      </View>
                      <View style={styles.corporatePanelOtherRows2}>
                        <Text style={styles.corporatePanelOtherRowsTitle}>
                          Brokerage Fee
                        </Text>
                        <Text style={styles.corporatePanelOtherRowsValues}>
                          {item.secondary_brokerage_fee}
                        </Text>
                      </View>
                    </View>
                    <View style={styles.corporatePanelEightRow}>
                      <View style={styles.corporatePanelOtherRows1}>
                        <Text
                          style={styles.corporatePanelOtherRowsTitle}
                        ></Text>
                        <Text
                          style={styles.corporatePanelOtherRowsValues}
                        ></Text>
                      </View>
                      <View style={styles.corporatePanelOtherRows2}>
                        <Text style={styles.corporatePanelOtherRowsTitle}>
                          Brokerage S+B
                        </Text>
                        <Text style={styles.corporatePanelOtherRowsValues}>
                          {item.total_s_b_fee}
                        </Text>
                      </View>
                    </View>
                  </View>
                )}
              />

              {/* <TouchableOpacity activeOpacity={0.7} style={styles.bottomView}>
                <MaterialCommunityIcons
                  name="download"
                  size={25}
                  color="#000000"
                ></MaterialCommunityIcons>

              </TouchableOpacity> */}
            </View>
          </View>
        </SafeAreaView>
      );
    }
  }
}

const styles = StyleSheet.create({
  loader: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#3E3E3E",
  },

  corporateProfitDate: {
    flex: 1,
    alignItems: "center",
  },

  imageView: {
    flex: 0.5,
    padding: 10,
    width: "15%",
    justifyContent: "flex-start",
  },

  image: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: "contain",
  },

  individualTopPanelViewPriceText: {
    color: "white",
    fontWeight: "bold",
  },

  individualTopPanelViewTitleText: {
    marginTop: 3,
    color: "white",
    fontSize: 12,
  },

  positive: {
    color: "#40FF49",
    fontSize: 13,
    fontWeight: "bold",
    padding: 3,
  },

  negative: {
    color: "#FF2800",
    fontSize: 13,
    fontWeight: "bold",
    padding: 3,
  },

  corporateDate: {
    color: "#FFFFFF",
    fontSize: 13,
    fontWeight: "bold",
    padding: 3,
  },

  corporateName: {
    flex: 1.5,
    justifyContent: "center",
    color: "#BD977E",
    fontSize: 15,
    marginLeft: 10,
    fontWeight: "bold",
    lineHeight: 20,
  },

  corporatePanelOtherRows1: {
    flexDirection: "row",
    borderColor: "white",
    borderRightWidth: 0.5,
    flex: 1,
  },
  corporatePanelOtherRows2: {
    flexDirection: "row",
    flex: 1,
  },

  corporatePanelOtherRowsTitle: {
    flex: 1,
    color: "#FFFFFF",
    alignSelf: "center",
    fontWeight: "bold",
    position: "absolute",
    left: 0,
    paddingLeft: 10,
    fontSize: RFPercentage(1.6),
  },
  corporatePanelOtherRowsValues: {
    flex: 1,
    color: "#FFFFFF",
    fontWeight: "bold",
    alignSelf: "center",
    position: "absolute",
    right: 0,
    paddingRight: 10,
    fontSize: RFPercentage(1.5),
  },

  corporatePanelFirstRow: {
    alignItems: "center",
    flexDirection: "row",
    flex: 2,
    borderColor: "white",
    borderBottomWidth: 0.5,
    paddingRight: 10,
  },

  corporatePanelSecondRow: {
    flexDirection: "row",
    flex: 1,
    // borderColor: "red",
    // borderBottomWidth: 0.5,
    //  paddingRight: 10,
  },
  corporatePanelThirdRow: {
    flexDirection: "row",
    flex: 1,
    //  borderColor: "white",
    //  borderBottomWidth: 0.5,
    //    paddingRight: 10,
  },
  corporatePanelFourthRow: {
    flexDirection: "row",
    flex: 1,
    borderColor: "white",
    borderBottomWidth: 0.5,
    // paddingRight: 10,
  },
  corporatePanelFifthRow: {
    flexDirection: "row",
    flex: 1,
    //  borderColor: "white",
    //  paddingRight: 10,
  },
  corporatePanelSixthRow: {
    flexDirection: "row",
    flex: 1,
    borderColor: "white",
    borderBottomWidth: 0.5,
    //  borderColor: "white",
    //  borderBottomWidth: 0.5,
    //    paddingRight: 10,
  },
  corporatePanelSeventhRow: {
    flexDirection: "row",
    flex: 1,
    //  borderColor: "white",
    //  borderBottomWidth: 0.5,
    //    paddingRight: 10,
  },
  corporatePanelEightRow: {
    flexDirection: "row",
    flex: 1,
    //  borderColor: "white",
    //  borderBottomWidth: 0.5,
    //    paddingRight: 10,
  },

  corporatePanel: {
    height: 270,
    //margin: 5,
    marginTop: 10,
    marginBottom: 10,
    borderColor: "white",
    borderWidth: 0.2,
    backgroundColor: "#313131",
  },

  individualTopPanelView: {
    flex: 1,
    //  margin: 5,
    marginTop: 5,
    marginBottom: 5,
    width: 130,
    height: 70,
    flexDirection: "column",
    borderColor: "white",
    borderWidth: 0.2,
    backgroundColor: "#313131",
    alignItems: "center",
    justifyContent: "center",
  },
  topPanelView: {
    flexDirection: "row",
    justifyContent: "space-evenly",
  },

  bottomText: {
    marginLeft: 7,
    color: "white",
  },

  bottomView: {
    padding: 5,
    margin: 10,
    marginBottom: 15,
    right: 0,
    backgroundColor: "#ebbea2",
    flexDirection: "row",
    width: 50,
    borderRadius: 50 / 2,
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    bottom: 0,
  },
});
