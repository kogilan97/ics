import * as React from "react";
import { render } from "react-dom";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  ScrollView,
  SafeAreaView,
  RefreshControl,
  ActivityIndicator,
  RecyclerViewBackedScrollView,
} from "react-native";
import { Table, Row, Rows, TableWrapper } from "react-native-table-component";
import axios from "axios";
import NetworkUtils from "../helpers/networkUtils";
import AsyncStorage from "@react-native-community/async-storage";

function firstColumnTextColour(value) {
  return <Text style={styles.firstColumnTextColour}>{value}</Text>;
}

function profitColumnTextColour(value) {
  var res = value.charAt(0);
  if (res == "+") {
    return <Text style={styles.postiveProfitTextColour}>{value}</Text>;
  } else if (res == "-") {
    return <Text style={styles.negativeProfitTextColour}>{value}</Text>;
  } else {
    return value;
  }
}

export default class InvestmentSummaryScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      newTableData: [],
      tableHead: ["FUND", "TOTAL", "PROFIT"],
      username: "",
      fullName: "",
      email: "",
      loading: false,
      title: "Pull down to refresh",
      refreshing: false,
      isConnected: true,
    };
  }

  onRefresh = () => {
    this.setState({ refreshing: true, loading: false });
    this.fetchData();
  };

  componentDidMount() {
    this.setState({
      loading: false,
    });
    this.fetchData();
  }

  async fetchData() {
    const isConnected = await NetworkUtils.isNetworkAvailable();

    AsyncStorage.multiGet(["user", "email", "firstName", "lastName"]).then(
      (data) => {
        const user_id = data[0][1];
        const email = data[1][1];
        const firstName = data[2][1];
        const lastName = data[3][1];

        if (data != null) {
          if (isConnected) {
            axios
              .post("http://live.intercapitas.com/api/profile", {
                user_id: user_id,
              })

              .then((response) => {
                let fundDetails = response.data.Profile;
                let refinedFundDetails = [];
                let username,
                  total = 0,
                  profit = 0;
                //     username = response.data.per_page;
                //     total = response.data.total;
                //     profit = profitColumnTextColour((response.data.total_pages).toString());
                for (var i = 0; i < fundDetails.length; i++) {
                  //    refinedFundDetails.push([firstColumnTextColour(fundDetails[i].email),fundDetails[i].first_name,fundDetails[i].last_name]);
                  total = total + parseFloat(fundDetails[i].total);
                  profit = profit + parseFloat(fundDetails[i].profit);
                  refinedFundDetails.push([
                    firstColumnTextColour(fundDetails[i].funds_name),
                    fundDetails[i].currency + " " + fundDetails[i].total,
                    profitColumnTextColour(
                      fundDetails[i].currency + " " + fundDetails[i].profit
                    ),
                  ]);
                }
                //      profit = profit.toFixed(2);
                //     total = total.toFixed(2);


                console.log(firstName + " "+ lastName)

                this.setState({
                  newTableData: refinedFundDetails,
                  //      username: username,
                  fullName: firstName + " " + lastName,
                  email: email,
                  loading: true,
                  refreshing: false,
                  isConnected: true,
                });
              })
              .catch((error) => {
                this.setState({
                  loading: true,
                  refreshing: false,
                  isConnected: true,
                });
                alert(error);
              });
          } else {
            this.setState({
              loading: true,
              refreshing: false,
              isConnected: false,
            });
          }
        }
      }
    );
  }

  render() {
    const state = this.state;
    if (!state.loading) {
      return (
        <View style={styles.loader}>
          <ActivityIndicator size="large" color="#BD977E" />
          <Text style={{ fontSize: 16, color: "white", marginTop: 20 }}>
            Loading Data...
          </Text>
        </View>
      );
    } else if (!state.isConnected) {
      return (
        <SafeAreaView style={{ flex: 1, backgroundColor: "#3E3E3E" }}>
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh}
                title="Pull to refresh"
              />
            }
          >
            <View style={styles.loader}>
              <Text
                style={{
                  fontSize: 16,
                  color: "white",
                  textAlign: "center",
                  marginTop: 20,
                }}
              >
                Please check your internet connection and pull down to refresh
              </Text>
            </View>
          </ScrollView>
        </SafeAreaView>
      );
    } else {
      return (
        <SafeAreaView style={{ flex: 1, backgroundColor: "#3E3E3E" }}>
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh}
                title="Pull to refresh"
              />
            }
          >
            <View style={{ flex: 1, paddingTop: 16 }}>
              <View
                style={{
                  flex: 1,
                }}
              >
                <View
                  style={{
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center",
                  }}
                >
                  <Text style={styles.fullName}>{state.fullName}</Text>
                </View>
                <View
                  style={{
                    marginTop: 8,
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center",
                  }}
                >
                  <Text style={styles.email}>{state.email}</Text>
                </View>

                <Table borderStyle={styles.table} style={{ marginTop: 20 }}>
                  <Row
                    data={state.tableHead}
                    style={styles.tableHeader}
                    textStyle={styles.tableHeaderText}
                  />
                </Table>
                <Table>
                  {state.newTableData.map((rowData, index) => (
                    <Row
                      key={index}
                      data={rowData}
                      style={styles.tableRow}
                      textStyle={styles.tableRowText}
                    />
                  ))}
                </Table>
              </View>
            </View>
          </ScrollView>
        </SafeAreaView>
      );
    }
  }
}

const styles = StyleSheet.create({
  loader: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#3E3E3E",
  },
  firstColumnTextColour: {
    margin: 6,
    color: "#BD977E",
    textAlign: "center",
    fontWeight: "bold",
  },

  postiveProfitTextColour: {
    margin: 6,
    color: "#40FF49",
    textAlign: "center",
    fontWeight: "bold",
  },

  negativeProfitTextColour: {
    margin: 6,
    color: "#FF2800",
    textAlign: "center",
    fontWeight: "bold",
  },

  tableHeader: {
    borderColor: "#FFFFFF",
    borderWidth: 0.5,
    borderStartColor: "transparent",
    borderTopColor: "transparent",
    borderEndColor: "transparent",
  },
  tableRow: {
    borderColor: "#FFFFFF",
    borderWidth: 0.5,
    borderStartColor: "transparent",
    borderTopColor: "transparent",
    borderEndColor: "transparent",
  },
  tableHeaderText: {
    margin: 6,
    color: "#FFFFFF",
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 12,
  },
  tableRowText: {
    margin: 12,
    color: "#FFFFFF",
    textAlign: "center",
    fontWeight: "bold",
  },

  title: {
    fontSize: 16,
    textAlign: "center",
    color: "#FFFFFF",
  },
  email: {
    fontSize: 16,
    textAlign: "center",
    color: "#40FF49",
  },

  fullName: {
    fontSize: 30,
    textAlign: "center",
    color: "#FFFFFF",
  },

  fullName: {
    fontWeight: "bold",
    fontSize: 19,
    textAlign: "center",
    color: "#FFFFFF",
    marginBottom: 10,
    marginTop: 10,
  },
});
