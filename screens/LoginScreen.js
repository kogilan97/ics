import React, { useState } from "react";
import { render } from "react-dom";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  ScrollView,
  StatusBar,
  SafeAreaView,
  ActivityIndicator,
  RecyclerViewBackedScrollView,
} from "react-native";
import Loader from "../helpers/loader";
import axios from "axios";
import XMLParser from "react-xml-parser";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import AsyncStorage from "@react-native-community/async-storage";

const storeData = async (key, value) => {
  try {
    await AsyncStorage.setItem(key, value);
  } catch (e) {
    console.log(e.toString());
  }
};

export default class LoginScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      secureTextEntry: true,
      iconName: "eye",
      textInputUsername: "",
      textInputPassword: "",
      loading: false,
    };
  }

  checkTextInput = () => {
    //Check for the Name TextInput

    if (!this.state.textInputUsername.trim()) {
      alert("Please Enter Username");
      return;
    }
    //Check for the Email TextInput
    if (!this.state.textInputPassword.trim()) {
      alert("Please Enter Password");
      return;
    }

    this.setState({
      loading: true,
    });

    axios
      .post("http://live.intercapitas.com/api/login", {
        email: this.state.textInputUsername,
        password: this.state.textInputPassword,
      })
      .then((response) => {
        setTimeout(() => {
          this.setState({
            loading: false,
          });
          if (response.data.response.response == "201") {
            storeData("user", response.data.user.id.toString());
            storeData("email", response.data.user.email.toString());
            storeData("firstName", response.data.user.firstName.toString());
            storeData("lastName", response.data.user.lastName.toString());
            storeData("referralCode", response.data.user.referral_code.toString());
            storeData("isUserProfile", response.data.user.is_userprofile.toString());

            if(!response.data.user.profile_image.toString().endsWith("/uploads/"))
             {
              console.log("EnteringImage")
              storeData("profileImage", response.data.user.profile_image.toString());
            }
            alert("You have been successfully verified.");
            this.props.navigation.replace("HomeScreen");
          } else {
            alert("Incorrect Username or Password");
          }
        }, 3000);
      })
      .catch((error) => {
        this.setState({
          loading: false,
        });
        alert("Something went wrong. Please try again. ");
      });

    //this.props.navigation.replace("HomeScreen");
  };

  onIconPress = () => {
    let iconName1 = this.state.secureTextEntry ? "eye-off" : "eye";
    this.setState({ secureTextEntry: !this.state.secureTextEntry });
    this.setState({ iconName: iconName1 });
  };

  render() {
    const state = this.state;

    if (state.loading) {
      return (
        <View style={styles.loader}>
          <ActivityIndicator size="large" color="#BD977E" />
          <Text style={{ fontSize: 16, color: "white", marginTop: 20 }}>
            Authenticating User...
          </Text>
        </View>
      );
    } else {
      return (
        <View style={styles.container}>
          <ScrollView
            keyboardShouldPersistTaps="handled"
            contentContainerStyle={{
              flexGrow: 1,
            }}
          >
            <StatusBar backgroundColor="#3E3E3E" barStyle="light-content" />
            <Image
              source={require("../src/images/icon_logo.png")}
              style={styles.logo}
            ></Image>

            <View style={styles.inputView}>
              <Image
                source={require("../src/images/user.png")}
                style={styles.Icon}
              ></Image>

              <TextInput
                style={styles.inputText}
                selectionColor="#BD977E"
                placeholder="Username"
                autoCapitalize="none"
                placeholderTextColor="#7e7e7e"
                onChangeText={(value) =>
                  this.setState({ textInputUsername: value })
                }
              />
            </View>
            <View style={styles.inputView}>
              <Image
                source={require("../src/images/lock.png")}
                style={styles.Icon}
              ></Image>
              <TextInput
                secureTextEntry={this.state.secureTextEntry}
                style={styles.inputText}
                selectionColor="#BD977E"
                placeholder="Password"
                 autoCapitalize="none"
                placeholderTextColor="#7e7e7e"
                onChangeText={(value) =>
                  this.setState({ textInputPassword: value })
                }
              />

              <MaterialCommunityIcons
                onPress={this.onIconPress}
                name={this.state.iconName}
                style={styles.eyeIcon}
                size={20}
                color="#FFFFFF"
              ></MaterialCommunityIcons>
            </View>

            <TouchableOpacity
              onPress={this.checkTextInput}
              style={styles.loginBtn}
            >
              <Text style={styles.loginText}>LOGIN</Text>
            </TouchableOpacity>
            {/* 
  
            <TouchableOpacity style={styles.forgotView}>
              <Text style={styles.forgot}>Forgot Password?</Text>
            </TouchableOpacity>
  
              */}
            <TouchableOpacity
              style={styles.bottomView}
              onPress={() => {
                this.props.navigation.navigate("User Registration");
              }}
            >
              <Text style={styles.bottomText}>
                Don't have an account? Register Now
              </Text>
            </TouchableOpacity>
          </ScrollView>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#3E3E3E",
    justifyContent: "center",
  },

  loader: {
    flex: 1,
    backgroundColor: "#3E3E3E",
    justifyContent: "center",
    alignItems: "center",
  },

  logo: {
    alignSelf: "center",
    marginTop: 80,
    marginBottom: 40,
    width: 150,
    height: 150,
  },

  Icon: {
    width: 30,
    height: 30,
  },

  eyeIcon: {
    width: 20,
    height: 20,
  },

  bottomText: {
    color: "white",
  },

  bottomView: {
    width: "100%",
    height: 50,
    borderTopWidth: 1.5,
    borderColor: "#BD977E",
    justifyContent: "center",
    alignItems: "center",
    position: "absolute", //Here is the trick
    bottom: 0, //Here is the trick
  },

  inputView: {
    alignSelf: "center",
    flexDirection: "row",
    width: "80%",
    borderBottomWidth: 1.5,
    borderColor: "#BD977E",
    height: 50,
    marginBottom: 20,
    justifyContent: "center",
    alignItems: "center",
    padding: 10,
  },

  inputText: {
    flex: 1,
    height: 50,
    marginStart: 10,
    color: "white",
  },
  forgot: {
    color: "white",
    fontSize: 15,
  },

  forgotView: {
    alignSelf: "center",
    marginBottom: 100,
  },

  loginBtn: {
    alignSelf: "center",
    width: "25%",
    backgroundColor: "#BD977E",
    borderRadius: 25,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    marginBottom: 100,
  },

  loginText: {
    fontSize: 15,
    fontWeight: "bold",
    color: "white",
  },
});
