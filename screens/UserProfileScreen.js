import * as React from "react";
import { render } from "react-dom";
import axios from "axios";
import NetworkUtils from "../helpers/networkUtils";
import Icon from "react-native-vector-icons/FontAwesome";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  SafeAreaView,
  RefreshControl,
  ActivityIndicator,
} from "react-native";
import { FlatGrid } from "react-native-super-grid";
import AsyncStorage from "@react-native-community/async-storage";

export default class UserProfileScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      refreshing: false,
      loading: true,
      isConnected: true,
      items: [
        {
          name: "Investment Summary",
          code: "#BD977E",
          stack_name: "Investment_Summary",
          icon: "file-text",
        },
        {
          name: "Registration Form",
          code: "#AF807D",
          stack_name: "Registration_Form",
          icon: "wpforms",
        },
        {
          name: "Upload Payment",
          stack_name: "Upload_Payment",
          code: "#BBA89C",
          icon: "money",
        },
        {
          name: "User Details",
          stack_name: "User_Details",
          code: "#99755D",
          icon: "user-circle-o",
        },
      ],
    };
  }

  render() {
    const state = this.state;
    if (!state.loading) {
      return (
        <View style={styles.loader}>
          <ActivityIndicator size="large" color="#BD977E" />
          <Text style={{ fontSize: 16, color: "white", marginTop: 20 }}>
            Loading Data...
          </Text>
        </View>
      );
    } else if (!state.isConnected) {
      return (
        <SafeAreaView style={{ flex: 1, backgroundColor: "#3E3E3E" }}>
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh}
                title="Pull to refresh"
              />
            }
          >
            <View style={styles.loader}>
              <Text
                style={{
                  fontSize: 16,
                  color: "white",
                  textAlign: "center",
                  marginTop: 20,
                }}
              >
                Please check your internet connection and pull down to refresh
              </Text>
            </View>
          </ScrollView>
        </SafeAreaView>
      );
    } else {
      return (
        <SafeAreaView style={{ flex: 1, backgroundColor: "#3E3E3E" }}>
          <FlatGrid
            itemDimension={150}
            data={state.items}
            style={styles.gridView}
            // staticDimension={300}
            // fixed
            spacing={15}
            renderItem={({ item }) => (
              <TouchableOpacity
                onPress={() => {
                  if (item.stack_name == "Registration_Form") {
                    AsyncStorage.getItem("isUserProfile").then((data) => {
                      if (data == "1") {
                        alert(
                          "You have already submitted the registration form."
                        );
                      } else {
                        this.props.navigation.navigate(item.stack_name);
                      }
                    });
                  } else if (item.stack_name == "User_Details") {
                    AsyncStorage.getItem("isUserProfile").then((data) => {
                      if (data == "0") {
                        alert(
                          "Please complete the registration form before viewing user profile."
                        );
                      } else {
                        this.props.navigation.navigate(item.stack_name);
                      }
                    });
                  } else {
                    this.props.navigation.navigate(item.stack_name);
                  }
                }}
                style={[styles.itemContainer, { backgroundColor: item.code }]}
              >
                <Icon
                  style={{ textAlign: "center" }}
                  name={item.icon}
                  size={80}
                  color="#fff"
                />

                <Text style={styles.itemName}>{item.name}</Text>
              </TouchableOpacity>
            )}
          />
        </SafeAreaView>
      );
    }
  }
}

const styles = StyleSheet.create({
  loader: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#3E3E3E",
  },

  logOut: {
    alignItems: "center",
    flex: 1,
    width: undefined,
    height: undefined,
  },

  individualIcon: {
    flex: 1,
    flexDirection: "row",
    textAlign: "center",
    alignItems: "center",
    justifyContent: "flex-start",
  },

  logOutView: {
    marginRight: 15,
    height: 35,
    width: 35,
  },

  gridView: {
    marginTop: 10,
    flex: 1,
  },
  itemContainer: {
    justifyContent: "center",
    paddingTop: 20,
    borderRadius: 5,
    padding: 10,
    height: 150,
  },
  itemName: {
    paddingTop: 13,
    fontSize: 16,
    color: "#fff",
    alignSelf: "center",
    fontWeight: "bold",
  },
});
