import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ImageBackground,
  ScrollView,
  TextInput,
  ToastAndroid,
  StyleSheet,
  ActivityIndicator,
  Platform,
  Alert,
} from "react-native";
import {
  Table,
  TableWrapper,
  Row,
  Rows,
  Col,
} from "react-native-table-component";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Feather from "react-native-vector-icons/Feather";
import axios from "axios";
import BottomSheet from "reanimated-bottom-sheet";
import Animated from "react-native-reanimated";
import DefaultImage from "../src/images/default_user.png";
import AsyncStorage from "@react-native-community/async-storage";
import Clipboard from "@react-native-community/clipboard";
import TextComponent from "./TextComponent";

const DEFAULT_IMAGE = Image.resolveAssetSource(DefaultImage).uri;

const DetailedProfileScreen1 = ({ navigation }) => {
  const [image, setImage] = useState(DEFAULT_IMAGE);
  const [userId, setUserId] = useState("");
  const [referralCode, setReferralCode] = useState("");
  const [tableHead, setTableHead] = useState([
    "First Name",
    "Last Name",
    "IC Number",
    "Contact Number",
    "Relationship",
    "Share",
  ]);

  const [nomineeDetails, setNomineeDetails] = useState([]);

  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [profileDetails, setProfileDetails] = useState(null);
  const [loading, setLoading] = useState(true);
  const [initialImage, setInitialImage] = useState("");
  const bs = React.createRef();
  const fall = new Animated.Value(1);

  const populateData = (userData) => {
    if (!userData.img_src.toString().endsWith("/uploads/")) {
      setImage(userData.img_src.toString());
    }
    var nomineeDetails = [];

    if (userData.nom_givenname) {
      console.log(userData.nom_givenname);

      nomineeDetails.push([
        userData.nom_givenname,
        userData.nom_lastname,
        userData.nom_nricpassdetails,
        userData.nom_contactnumber,
        userData.nom_relationship,
        userData.nom_share,
      ]);
    }

    if (userData.nom_givenname2) {
      nomineeDetails.push([
        userData.nom_givenname2,
        userData.nom_lastname2,
        userData.nom_nricpassdetails2,
        userData.nom_contactnumber2,
        userData.nom_relationship2,
        userData.nom_share2,
      ]);
    }

    if (userData.nom_givenname3) {
      nomineeDetails.push([
        (userData.nom_givenname3,
        userData.nom_lastname3,
        userData.nom_nricpassdetails3,
        userData.nom_contactnumber3,
        userData.nom_relationship3,
        userData.nom_share3),
      ]);
    }
    setNomineeDetails(nomineeDetails);
    setProfileDetails(userData);
    setLoading(false);
  };

  useEffect(() => {
    AsyncStorage.multiGet(["user", "firstName", "lastName"]).then((data) => {
      const user_id = data[0][1];
      const firstName = data[1][1];
      const lastName = data[2][1];
      setFirstName(firstName);
      setLastName(lastName);
      console.log(firstName + " " +lastName)

      if (data != null) {
        axios
          .post("http://live.intercapitas.com/api/userprofile", {
            user_id: user_id,
          })
          .then((response) => populateData(response.data.userprofile))
          .catch((error) => {
            console.log(error);
            if (error.response) {
              alert(
                " Your application is received and is under verification. Please try again later"
              );
            } else {
              alert("Something went wrong. Please try again");
            }

            navigation.goBack();
          });
      }
    });
  }, []);

  const updateUserDetails = () => {
    console.log(password + " || " + confirmPassword);
    if (password.trim() != confirmPassword.trim()) {
      alert("Password and confirm password does not match");
      return;
    }

    if (password.trim().length < 8) {
      alert("The password must be more than 8 characters.");
      return;
    }

    setLoading(true);

    var data = new FormData();
    console.log(profileDetails.user_id);
    data.append("user_id", profileDetails.user_id);
    if (password.trim()) {
      data.append("password", password);
    }
    data.append("firstname",firstName ? firstName : "")
    data.append("lastname",lastName ? lastName : "")

    axios
      .post("http://live.intercapitas.com/api/userupdate", data)
      .then((response) => {
        setLoading(false);
        if (response.data.response.response == "200") {
          alert(response.data.response.message);
          navigation.goBack();
        }
      })
      .catch((error) => {
        console.log(error);
        setLoading(false);
        alert("Something went wrong. Please try again. ");
      });
  };

  if (loading) {
    return (
      <View style={styles.loader}>
        <ActivityIndicator size="large" color="#BD977E" />
        <Text style={{ fontSize: 16, color: "white", marginTop: 20 }}>
          Loading...
        </Text>
      </View>
    );
  }

  return (
    <ScrollView
      keyboardShouldPersistTaps="handled"
      contentContainerStyle={{
        flexGrow: 1,
      }}
    >
      <View style={styles.container}>
        <View style={{ alignItems: "center" }}>
          <View
            style={{
              height: 100,
              width: 100,
              borderRadius: 15,
              justifyContent: "center",
              alignItems: "center",
              marginTop: 20,
            }}
          >
            <ImageBackground
              source={{
                uri: image,
              }}
              style={{ height: 100, width: 100 }}
              imageStyle={{ borderRadius: 15 }}
            ></ImageBackground>
          </View>

          <TouchableOpacity
            onLongPress={() => {
              Clipboard.setString(referralCode);
              alert("Your referral code has been copied to clipboard");
            }}
            style={{
              borderColor: "#fff",
              borderWidth: 0.5,
              padding: 10,
              marginVertical: 15,
              flexDirection: "row",
            }}
          >
            <Text
              style={{
                fontSize: 15,
                color: "#fff",
              }}
            >
              Your Referral Code :{" "}
            </Text>
            <Text
              style={{
                fontSize: 15,
                fontStyle: "italic",
                fontWeight: "bold",
                color: "#fff",
              }}
            >
              {profileDetails.referral_code}
            </Text>
          </TouchableOpacity>
        </View>

        <Text style={styles.mainTitle}>Personal Details</Text>

        <TextComponent
          title="Name"
          value={profileDetails.name}
          editable={false}
        />
        <TextComponent
          title="New IC"
          value={profileDetails.new_nric}
          editable={false}
        />
        <TextComponent
          title="Old IC"
          value={profileDetails.old_nric_pass_no}
          editable={false}
        />
        <TextComponent
          title="Gender"
          value={profileDetails.gender}
          editable={false}
        />
        <TextComponent
          title="Residential Status"
          value={profileDetails.residentstatus}
          editable={false}
        />
        <TextComponent
          title="Nationality"
          value={profileDetails.nationality}
          editable={false}
        />
        <TextComponent
          title="Marital Status"
          value={profileDetails.maritalstatus}
          editable={false}
        />
        <TextComponent
          title="Race"
          value={profileDetails.race}
          editable={false}
        />
        <TextComponent
          title="Date of Birth"
          value={profileDetails.dob}
          editable={false}
        />

        <Text style={styles.subTitle}>Residential</Text>

        <TextComponent
          title="Address 1"
          value={profileDetails.residentaddr1}
          editable={false}
        />

        <TextComponent
          title="Address 2"
          value={profileDetails.residentaddr2}
          editable={false}
        />

        <TextComponent
          title="Postcode"
          value={profileDetails.residentpostcode}
          editable={false}
        />

        <Text style={styles.subTitle}>Correspondence</Text>

        <TextComponent
          title="Address 1"
          value={profileDetails.corresaddress1}
          editable={false}
        />

        <TextComponent
          title="Address 2"
          value={profileDetails.corresaddress2}
          editable={false}
        />

        <TextComponent
          title="Postcode"
          value={profileDetails.correspostcode}
          editable={false}
        />

        <TextComponent
          title="Home Tel"
          value={profileDetails.hometel ? profileDetails.hometel : "-"}
          editable={false}
        />

        <TextComponent
          title="Mobile Tel"
          value={profileDetails.mobtel}
          editable={false}
        />

        <TextComponent
          title="Email"
          value={profileDetails.email}
          editable={false}
        />

        <TextComponent
          title="Fax"
          value={profileDetails.fax ? profileDetails.fax : "-"}
          editable={false}
        />

        <Text style={styles.mainTitle}>Employment Details</Text>

        <TextComponent
          title="Employment"
          value={profileDetails.employment}
          editable={false}
        />

        <TextComponent
          title="Company Name"
          value={profileDetails.companyname}
          editable={false}
        />

        <TextComponent
          title="Designation"
          value={profileDetails.designation}
          editable={false}
        />

        <Text style={styles.mainTitle}>Emergency Contact Person</Text>

        <TextComponent
          title="Name"
          value={profileDetails.emergencyname}
          editable={false}
        />

        <TextComponent
          title="Relationship"
          value={profileDetails.relationship}
          editable={false}
        />

        <TextComponent
          title="Mobile Tel"
          value={profileDetails.mobiletel}
          editable={false}
        />

        <Text style={styles.mainTitle}>Bank Account Details (For Refund)</Text>

        <TextComponent
          title="Bank"
          value={profileDetails.accountno}
          editable={false}
        />

        <TextComponent
          title="Account No"
          value={profileDetails.accountno}
          editable={false}
        />

        <TextComponent
          title="Account Type"
          value={profileDetails.accountype}
          editable={false}
        />

        <Text style={styles.mainTitle}>Nominee(s)' Details</Text>

        <ScrollView style={styles.dataWrapper} horizontal={true}>
          <View>
            <ScrollView>
              <Table>
                <Row
                  data={tableHead}
                  width={120}
                  style={styles.tableHeader}
                  textStyle={styles.tableHeaderText}
                />
                {nomineeDetails.map((rowData, index) => (
                  <Row
                    key={index}
                    data={rowData}
                    width={120}
                    style={styles.tableRow}
                    textStyle={styles.tableRowText}
                  />
                ))}
              </Table>
            </ScrollView>
          </View>
        </ScrollView>

        <Text style={styles.mainTitle}>Investment Objectives</Text>

        <TextComponent
          title="Investment and Objectives"
          value={profileDetails.investmentobj}
          editable={false}
        />

        <TextComponent
          title="Risk Tolerance"
          value={profileDetails.risktolerance}
          editable={false}
        />

        <TextComponent
          title="Investment Group"
          value={profileDetails.investgroup}
          editable={false}
        />

        <TextComponent
          title="Investment Amount"
          value={profileDetails.investamount}
          editable={false}
        />

        <TextComponent
          title="Agent Code"
          value={profileDetails.investagent}
          editable={false}
        />

        <Text style={styles.mainTitle}>Password</Text>

        <TextComponent
          title="Password"
          editable
          secureTextEntry
          onSetCallback={setPassword}
        />

        <TextComponent
          title="Confirm Password"
          editable
          secureTextEntry
          onSetCallback={setConfirmPassword}
        />

        <TouchableOpacity
          onPress={() => {
            updateUserDetails();
          }}
          style={styles.commandButton}
        >
          <Text style={styles.panelButtonTitle}>Change Password</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  tableHeader: {
    height: 40,
    backgroundColor: "#474747",
    borderColor: "#FFFFFF",
    borderWidth: 0.5,
    borderStartColor: "transparent",
    borderTopColor: "transparent",
    borderEndColor: "transparent",
  },

  dataWrapper: {
    width: "90%",
    alignSelf: "center",
    marginBottom: 15,
  },

  tableRow: {
    height: 30,
    borderColor: "#FFFFFF",
    backgroundColor: "#474747",
  },

  tableHeaderText: {
    margin: 6,
    color: "#BD977E",
    textAlign: "center",
    fontSize: 12,
  },

  tableRowText: {
    margin: 6,
    color: "#FFFFFF",
    textAlign: "center",
  },

  container: {
    backgroundColor: "#3E3E3E",
    flex: 1,
  },

  commandButton: {
    alignSelf: "center",
    width: "45%",
    backgroundColor: "#BD977E",
    borderRadius: 25,
    height: 45,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    marginBottom: 20,
  },
  panel: {
    padding: 20,
    backgroundColor: "#FFFFFF",
    paddingTop: 20,
  },

  panelButtonTitle: {
    fontSize: 17,
    fontWeight: "bold",
    color: "white",
  },

  loader: {
    flex: 1,
    backgroundColor: "#3E3E3E",
    justifyContent: "center",
    alignItems: "center",
  },

  mainTitle: {
    color: "#FFF",
    fontSize: 22,
    alignSelf: "center",
    marginVertical: 20,
    textAlign: "center",
  },
  subTitle: {
    color: "#FFF",
    fontSize: 18,
    alignSelf: "center",
    //    marginLeft: 40,
    textDecorationLine: "underline",
    marginBottom: 15,
  },
});

export default DetailedProfileScreen1;
