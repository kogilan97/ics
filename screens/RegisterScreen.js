import React, { useState } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  ImageBackground,
  ScrollView,
  Image,
  TextInput,
  ActivityIndicator,
  StyleSheet,
  Platform,
} from "react-native";

import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Feather from "react-native-vector-icons/Feather";
import BottomSheet from "reanimated-bottom-sheet";
import Animated from "react-native-reanimated";
import axios from "axios";
import ImagePicker from "react-native-image-crop-picker";
import DefaultImage from "../src/images/default_user.png";

const DEFAULT_IMAGE = Image.resolveAssetSource(DefaultImage).uri;

const RegisterScreen = ({ navigation }) => {
  const [image, setImage] = useState(DEFAULT_IMAGE);
  const [referralCode, setReferralCode] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [loading, setLoading] = useState(false);

  const bs = React.createRef();
  const fall = new Animated.Value(1);

  const takePhotoFromCamera = () => {
    ImagePicker.openCamera({
      compressImageMaxWidth: 300,
      compressImageMaxHeight: 300,
      cropping: true,
      compressImageQuality: 0.7,
    }).then((image) => {
      console.log(image);
      bs.current.snapTo(1);
      setImage(image.path);
    })
    .catch((error)=>{

      if (error.message.toString().startsWith("User did not grant"))
      {
        alertDialog(
          'Permission Denied',
          'You are required to give permission if you wish to upload your profile picture',
        );
      }

      bs.current.snapTo(1);
    });
  };

  const choosePhotoFromLibrary = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 300,
      cropping: true,
      compressImageQuality: 0.7,
    }).then((image) => {
      console.log(image);
      bs.current.snapTo(1);
      setImage(image.path);
    })
    .catch((error)=>{

      if (error.message.toString().startsWith("User did not grant"))
      {
        alertDialog(
          'Permission Denied',
          'You are required to give permission if you wish to upload your profile picture',
        );
      }

      bs.current.snapTo(1);
    });
  };

  const registerUser = () => {
    if (
      !firstName.trim() ||
      !lastName.trim() ||
      !password.trim() ||
      !confirmPassword.trim() ||
      !email.trim()
    ) {
      alert("Please Enter All The Required Details");
      return;
    }

    if (password.trim() != confirmPassword.trim()) {
      alert("Password and confirm password does not match");
      return;
    }

    setLoading(true);

    var data = new FormData();

    data.append("firstname", firstName);
    data.append("lastname", lastName);
    data.append("email", email);
    data.append("password", password);

    if (referralCode.trim()) {
      data.append("referrer_code", referralCode);
    }

    if (image != DEFAULT_IMAGE) {
      console.log("Entering2");
      data.append("userfile", {
        uri: image,
        name: "userProfile.jpg",
        type: "image/jpg",
      });
    }

    axios
      .post("http://live.intercapitas.com/api/register", data)
      .then((response) => {
        setLoading(false);
        console.log("Entering Response");
        if (response.data.response.response == "200") {
          alert(response.data.response.message);
          navigation.goBack();
        }
      })
      .catch((error) => {
        setLoading(false);
        setConfirmPassword("");
        setPassword("");
        setFirstName("");
        setLastName("");
        setReferralCode("");
        setEmail("");
        setImage(DEFAULT_IMAGE);

        if (error.response.data) {
          if (error.response.data.response.response == "401") {
            if (error.response.data.response.validator) {
              if (error.response.data.response.validator.firstname) {
                alert(error.response.data.response.validator.firstname);
                return;
              }
              if (error.response.data.response.validator.email) {
                alert(error.response.data.response.validator.email);
                return;
              }

              if (error.response.data.response.validator.password) {
                alert(error.response.data.response.validator.password);
                return;
              }
            }
            else if (error.response.data.response.message) {
              alert(error.response.data.response.message);
              return;
            }
          }
        }
        alert("Something went wrong. Please try again. ");
      });
  };

  const renderInner = () => (
    <View style={styles.panel}>
      <View style={{ alignItems: "center" }}>
        <Text style={styles.panelTitle}>Upload Photo</Text>
        <Text style={styles.panelSubtitle}>Choose Your Profile Picture</Text>
      </View>
      <TouchableOpacity
        style={styles.panelButton}
        onPress={takePhotoFromCamera}
      >
        <Text style={styles.panelButtonTitle}>Take Photo</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.panelButton}
        onPress={choosePhotoFromLibrary}
      >
        <Text style={styles.panelButtonTitle}>Choose From Library</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.panelButton}
        onPress={() => bs.current.snapTo(1)}
      >
        <Text style={styles.panelButtonTitle}>Cancel</Text>
      </TouchableOpacity>
    </View>
  );

  const renderHeader = () => (
    <View style={styles.header}>
      <View style={styles.panelHeader}>
        <View style={styles.panelHandle} />
      </View>
    </View>
  );

  if (loading) {
    return (
      <View style={styles.loader}>
        <ActivityIndicator size="large" color="#BD977E" />
        <Text style={{ fontSize: 16, color: "white", marginTop: 20 }}>
          Registering User...
        </Text>
      </View>
    );
  }

  return (
    <ScrollView
      keyboardShouldPersistTaps="handled"
      contentContainerStyle={{
        flexGrow: 1,
      }}
    >
      <View style={styles.container}>
        <BottomSheet
          ref={bs}
          snapPoints={[330, 0]}
          renderContent={renderInner}
          renderHeader={renderHeader}
          initialSnap={1}
          callbackNode={fall}
          initialSnap={1}
          enabledContentGestureInteraction={false}
          enabledGestureInteraction={true}
        />
        <Animated.View
          style={{
            marginVertical: 20,
            opacity: Animated.add(0.1, Animated.multiply(fall, 1.0)),
          }}
        >
          <View style={{ alignItems: "center", marginBottom: 20 }}>
            <TouchableOpacity onPress={() => bs.current.snapTo(0)}>
              <View
                style={{
                  height: 100,
                  width: 100,
                  borderRadius: 15,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <ImageBackground
                  source={{
                    uri: image,
                  }}
                  style={{ height: 100, width: 100 }}
                  imageStyle={{ borderRadius: 15 }}
                >
                  <View
                    style={{
                      flex: 1,
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <Icon
                      name="camera"
                      size={35}
                      color="#fff"
                      style={{
                        opacity: 0.7,
                        alignItems: "center",
                        justifyContent: "center",
                        borderWidth: 1,
                        borderColor: "#fff",
                        borderRadius: 10,
                      }}
                    />
                  </View>
                </ImageBackground>
              </View>
            </TouchableOpacity>
          </View>

          <View style={styles.action}>
            <FontAwesome name="user-o" color="#fff" size={20} />
            <TextInput
              placeholder="Referral ID (Optional)"
              selectionColor="#BD977E"
              placeholderTextColor="#7e7e7e"
              onChangeText={(value) => {
                setReferralCode(value);
              }}
              autoCorrect={false}
              style={[
                styles.textInput,
                {
                  color: "#fff",
                },
              ]}
            />
          </View>

          <View style={styles.action}>
            <FontAwesome name="user-o" color="#fff" size={20} />
            <TextInput
              placeholder="First Name"
              selectionColor="#BD977E"
              onChangeText={(value) => {
                setFirstName(value);
              }}
              placeholderTextColor="#7e7e7e"
              autoCorrect={false}
              style={[
                styles.textInput,
                {
                  color: "#fff",
                },
              ]}
            />
          </View>
          <View style={styles.action}>
            <FontAwesome name="user-o" color="#fff" size={20} />
            <TextInput
              placeholder="Last Name"
              placeholderTextColor="#7e7e7e"
              onChangeText={(value) => {
                setLastName(value);
              }}
              selectionColor="#BD977E"
              autoCorrect={false}
              style={[
                styles.textInput,
                {
                  color: "#fff",
                },
              ]}
            />
          </View>

          <View style={styles.action}>
            <FontAwesome name="envelope-o" color="#fff" size={20} />
            <TextInput
              placeholder="Email"
              placeholderTextColor="#7e7e7e"
              autoCapitalize="none"
              onChangeText={(value) => {
                setEmail(value);
              }}
              selectionColor="#BD977E"
              keyboardType="email-address"
              autoCorrect={false}
              style={[
                styles.textInput,
                {
                  color: "#fff",
                },
              ]}
            />
          </View>

          <View style={styles.action}>
            <Feather name="lock" color="#fff" size={20} />
            <TextInput
              secureTextEntry
              onChangeText={(value) => {
                setPassword(value);
              }}
              placeholder="Password"
              autoCapitalize="none"
              placeholderTextColor="#7e7e7e"
              selectionColor="#BD977E"
              autoCorrect={false}
              style={[
                styles.textInput,
                {
                  color: "#fff",
                },
              ]}
            />
          </View>

          <View style={styles.action}>
            <Feather name="lock" color="#fff" size={20} />
            <TextInput
              placeholder="Confirm Password"
              placeholderTextColor="#7e7e7e"
              autoCapitalize="none"
              onChangeText={(value) => {
                setConfirmPassword(value);
              }}
              secureTextEntry
              selectionColor="#BD977E"
              autoCorrect={false}
              style={[
                styles.textInput,
                {
                  color: "#fff",
                },
              ]}
            />
          </View>

          <TouchableOpacity onPress={registerUser} style={styles.commandButton}>
            <Text style={styles.panelButtonTitle}>Submit</Text>
          </TouchableOpacity>
        </Animated.View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#3E3E3E",
    flex: 1,
  },
  commandButton: {
    alignSelf: "center",
    width: "35%",
    backgroundColor: "#BD977E",
    borderRadius: 25,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    marginBottom: 20,
  },
  panel: {
    padding: 20,
    backgroundColor: "#FFFFFF",
    paddingTop: 20,
  },
  header: {
    backgroundColor: "#FFFFFF",
    shadowColor: "#333333",
    shadowOffset: { width: -1, height: -3 },
    shadowRadius: 2,
    shadowOpacity: 0.4,
    // elevation: 5,
    paddingTop: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  panelHeader: {
    alignItems: "center",
  },
  panelHandle: {
    width: 40,
    height: 8,
    borderRadius: 4,
    backgroundColor: "#00000040",
    marginBottom: 10,
  },
  panelTitle: {
    fontSize: 27,
    height: 35,
  },
  panelSubtitle: {
    fontSize: 14,
    color: "gray",
    height: 30,
    marginBottom: 10,
  },
  panelButton: {
    padding: 13,
    borderRadius: 10,
    backgroundColor: "#BD977E",
    alignItems: "center",
    marginVertical: 7,
  },
  panelButtonTitle: {
    fontSize: 17,
    fontWeight: "bold",
    color: "white",
  },
  action: {
    alignSelf: "center",
    flexDirection: "row",
    width: "80%",
    borderBottomWidth: 1.5,
    borderColor: "#BD977E",
    height: 50,
    marginBottom: 20,
    justifyContent: "center",
    alignItems: "center",
    padding: 10,
  },
  actionError: {
    flexDirection: "row",
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#FF0000",
    paddingBottom: 5,
  },
  textInput: {
    flex: 1,
    height: 50,
    marginStart: 10,
    color: "white",
  },
  loader: {
    flex: 1,
    backgroundColor: "#3E3E3E",
    justifyContent: "center",
    alignItems: "center",
  },
});

export default RegisterScreen;
